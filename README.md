# Final Project JCC Node JS Batch 1

# Data Individu:
  - Nama  : Fajar Hikmal Gunawan
  - Email : fajarhikmal214@gmail.com

# Link deploy heroku

- BASE URL : https://jcc-api-main-bersama.herokuapp.com
- Dokumentasi : https://jcc-api-main-bersama.herokuapp.com/docs

# Tips

- Jika kode OTP tidak diterima, cobalah mengganti dengan email baru dan coba registrasi kembali 
- Jika ingin menggunakan filter, gunakan masukkan sesuai dengan data termasuk hurup besar dan huruf kecilnya (case-sensitive) karena sedang menggunakan RDBMS **PosgreeSQL**
