/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";
import HealthCheck from '@ioc:Adonis/Core/HealthCheck'

Route.get("/", async ({ response }) => {
  const report = await HealthCheck.getReport()
  return report.healthy ? response.ok(report) : response.badRequest(report)
});

Route.group(() => {
  Route.group(() => {
    Route.group(() => {
      Route.resource("/venues", "VenuesController").apiOnly();
      Route.resource("/venues.fields", "FieldsController").apiOnly();
    }).middleware("role:owner");

    Route.post("venues/:id/bookings", "VenuesController.booking")
      .as("venues.booking")
      .middleware("role:user");

    Route.group(() => {
      Route.group(() => {
        Route.get("/", "BookingsController.index").as("bookings.index");
        Route.get("/:id", "BookingsController.show").as("bookings.show");
      }).middleware("role:user,owner");
      Route.group(() => {
        Route.put("/:id", "BookingsController.update").as("bookings.update");
        Route.delete("/:id", "BookingsController.destroy").as(
          "bookings.destroy"
        );
      }).middleware("role:user");
      Route.group(() => {
        Route.put("/:id/join", "BookingsController.join").as("bookings.join");
        Route.put("/:id/unjoin", "BookingsController.unjoin").as(
          "bookings.unjoin"
        );
      }).middleware("role:user");
    }).prefix("bookings");

    Route.get("/schedules", "BookingsController.schedules")
      .as("bookings.schedules")
      .middleware("role:user");
  }).middleware(["auth", "verify"]);

  Route.post("/register", "AuthController.register").as("auth.register");
  Route.post("/login", "AuthController.login").as("auth.login");
  Route.post("/otp-confirmation", "AuthController.otpConfirmation").as(
    "auth.otpConfirmation"
  );
}).prefix("api/v1");
