import Mail from "@ioc:Adonis/Addons/Mail";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import OtpCode from "App/Models/OtpCode";
import User from "App/Models/User";
import OtpConfirmationValidator from "App/Validators/OtpConfirmationValidator";
import UserLoginValidator from "App/Validators/UserLoginValidator";
import UserRegisterValidator from "App/Validators/UserRegisterValidator";

export default class AuthController {
  /**
   * @swagger
   * /api/v1/register:
   *   post:
   *     tags:
   *       - Authentication
   *     summary: Register
   *     requestBody:
   *       required: true
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             $ref: '#definitions/User'
   *     responses:
   *       '201':
   *          description: Register berhasil dilakukan, Silakan verifikasi dengan kode OTP anda
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async register({ request, response }: HttpContextContract) {
    try {
      const { name, email, password, role } = request.body();
      await request.validate(UserRegisterValidator);
      const user = await User.create({
        name,
        email,
        password,
        role,
      });

      const otp_code = Math.floor(100000 + Math.random() * 900000);

      const otpCode = new OtpCode();
      otpCode.otp_code = otp_code;
      await otpCode.related("owner").associate(user);

      await Mail.send((message) => {
        message
          .from("adonis.demo@sanberdev.com")
          .to(email)
          .subject("Verifikasi Akun")
          .htmlView("emails/otp-verification", { otp_code });
      });

      return response.status(201).json({
        status: "success",
        messages:
          "Register berhasil dilakukan, Silakan verifikasi dengan kode OTP anda",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Register gagal dilakukan",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Register gagal dilakukan",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/login:
   *   post:
   *     tags:
   *       - Authentication
   *     summary: Login
   *     requestBody:
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             type: object
   *             properties:
   *               email:
   *                 description: User email
   *                 type: string
   *               password:
   *                 description: User password
   *                 type: string
   *             required:
   *               - email
   *               - password
   *     responses:
   *       '200':
   *          description: Login berhasil dilakukan
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '401':
   *         description: Email atau password tidak sesuai.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async login({ auth, request, response }: HttpContextContract) {
    try {
      const data = await request.validate(UserLoginValidator);
      const token = await auth.use("api").attempt(data.email, data.password);

      return response.ok({
        status: "success",
        messages: "Login berhasil dilakukan",
        token,
      });
    } catch (error) {
      if (error.code === "E_INVALID_AUTH_UID") {
        return response.unauthorized({
          status: "failed",
          messages: "Login gagal dilakukan",
          error: error.message,
        });
      } else if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Login gagal dilakukan",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Login gagal dilakukan",
          error: error.message,
        });
      }
    }
  }
  /**
   * @swagger
   * /api/v1/otp-confirmation:
   *   post:
   *     tags:
   *       - Authentication
   *     summary: Konfirmasi Akun OTP
   *     requestBody:
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             type: object
   *             properties:
   *               otp_code:
   *                 description: Konfirmasi kode OTP yang diterima melalui email
   *                 type: string
   *               email:
   *                 description: User email
   *                 type: string
   *             required:
   *               - otp_code
   *               - email
   *     responses:
   *       '200':
   *          description: Konfirmasi OTP berhasil dilakukan
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async otpConfirmation({ request, response }: HttpContextContract) {
    try {
      const data = await request.validate(OtpConfirmationValidator);

      const user = await User.findByOrFail("email", data.email);
      const otpCheck = await OtpCode.findBy("otp_code", data.otp_code);

      if (user.id === otpCheck?.users_id) {
        user.is_verified = true;
        await user.save();

        return response.ok({
          status: "success",
          messages: "Konfirmasi OTP berhasil dilakukan",
        });
      } else {
        return response.badRequest({
          status: "failed",
          messages: "Kode OTP tidak sesuai",
        });
      }
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Konfirmasi OTP gagal dilakukan",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Konfirmasi OTP gagal dilakukan",
          error: error.message,
        });
      }
    }
  }
}
