import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import CreateVenueValidator from "App/Validators/CreateVenueValidator";
import Venue from "App/Models/Venue";
import User from "App/Models/User";
import FieldBookingValidator from "App/Validators/FieldBookingValidator";
import Field from "App/Models/Field";
import Booking from "App/Models/Booking";
import { Exception } from "@poppinss/utils";

export default class VenuesController {
  /**
   * @swagger
   * /api/v1/venues:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Menampilkan semua data venues
   *     parameters:
   *       - in: query
   *         name: name
   *         schema:
   *           type: string
   *     responses:
   *       '200':
   *          description: Venues berhasil dimuat
   *       '5XX':
   *         description: Unexpected error.
   */
  public async index({ request, response }: HttpContextContract) {
    try {
      if (request.qs().name) {
        const venues = await Venue.query()
          .where("name", "LIKE", "%" + request.qs().name + "%")
          .preload("author")
          .preload("fields");
        return response.ok({
          status: "success",
          message: "Venues berhasil dimuat",
          data: venues,
        });
      } else {
        const venues = await Venue.query().preload("author").preload("fields");
        return response.ok({
          status: "success",
          message: "Venues berhasil dimuat",
          data: venues,
        });
      }
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Venues gagal dimuat",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/venues:
   *   post:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Membuat data venue baru
   *     requestBody:
   *       required: true
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             $ref: '#definitions/Venue'
   *     responses:
   *       '201':
   *          description: Venues berhasil ditambahkan
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async store({ auth, request, response }: HttpContextContract) {
    try {
      const { name, address, phone } = request.body();
      await request.validate(CreateVenueValidator);

      const user_id = auth.user!.id;
      const user = await User.findByOrFail("id", user_id);

      const venue = new Venue();
      venue.name = name;
      venue.address = address;
      venue.phone = phone;
      await venue.related("author").associate(user);

      return response.created({
        status: "success",
        message: "Venue berhasil ditambahkan",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Venue gagal ditambahkan",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Venue gagal ditambahkan",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{id}:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Menampilkan venue by id
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Venue berhasil dimuat
   *       '5XX':
   *         description: Unexpected error.
   */
  public async show({ params, response }: HttpContextContract) {
    try {
      const { id } = params;
      const venue = await Venue.query()
        .where("id", id)
        .preload("author")
        .preload("fields")
        .first();

      return response.ok({
        status: "success",
        message: "Venue berhasil dimuat",
        data: venue,
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Venue gagal dimuat",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{id}:
   *   put:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Mengubah venue by ID
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     requestBody:
   *       required: true
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             $ref: '#definitions/Venue'
   *     responses:
   *       '200':
   *          description: Venues berhasil diperbarui
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async update({ auth, request, response }: HttpContextContract) {
    try {
      const { id } = request.params();
      const { name, address, phone } = request.body();

      const user_id = auth.user!.id;
      const user = await User.findByOrFail("id", user_id);

      let venue = await Venue.findOrFail(id);
      venue.name = name;
      venue.address = address;
      venue.phone = phone;
      await venue.related("author").associate(user);

      return response.ok({
        status: "success",
        message: "Venue berhasil diperbarui",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Venue gagal diperbarui",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Venue gagal diperbarui",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{id}:
   *   delete:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Menghapus data venue by id
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Venue berhasil dihapus
   *       '5XX':
   *         description: Unexpected error.
   */
  public async destroy({ params, response }: HttpContextContract) {
    try {
      const { id } = params;

      let venue = await Venue.findOrFail(id);
      await venue.delete();

      return response.ok({
        status: "success",
        message: "Venue berhasil dihapus",
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Venue gagal dihapus",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{id}/bookings:
   *   post:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Venue
   *     summary: Melakukan booking pada field
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     requestBody:
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             type: object
   *             properties:
   *               fields_id:
   *                 description: Field ID yang akan dibooking
   *                 type: integer
   *               play_date_start:
   *                 description: Tanggal dan Waktu mulai bermain, dalam format YYYY-mm-dd HH:MM:SS
   *                 type: datetime
   *               play_date_end:
   *                 description: Tanggal dan Waktu selesai bermain, dalam format YYYY-mm-dd HH:MM:SS
   *                 type: datetime
   *             required:
   *               - fields_id
   *               - play_date_start
   *               - play_date_end
   *     responses:
   *       '201':
   *          description: Berhasil booking
   *       '404':
   *          description: Field ID tidak tersedia
   *       '5XX':
   *         description: Unexpected error.
   */
  public async booking({ auth, request, response }: HttpContextContract) {
    try {
      const { id } = request.params();
      const { fields_id, play_date_start, play_date_end } = request.body();

      let venue = await Venue.findOrFail(id);

      if (!venue) {
        throw new Exception("Venue ID tidak tersedia", 422);
      }

      await request.validate(FieldBookingValidator);

      const user = auth.user;
      const field = await Field.findOrFail(fields_id);

      const isAlreadyBooked = await Booking.query()
        .where({
          fields_id: fields_id,
          play_date_start: play_date_start,
        })
        .first();

      if (isAlreadyBooked) {
        throw new Exception(
          `Lapangan ${field.name} pada ${play_date_start} sudah terbooking, silakan cari lapangan atau waktu lain`,
          422
        );
      }

      await field.related("bookings").create({
        play_date_start,
        play_date_end,
        users_id: user?.id,
      });

      return response.created({
        status: "success",
        message: "berhasil booking",
      });
    } catch (error) {
      console.log(error.status);
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "faield",
          error: error.messages,
        });
      } else if (error.status == 422) {
        return response.unprocessableEntity({
          status: "failed",
          message: error.message,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Venue gagal dihapus",
          error: error.message,
        });
      }
    }
  }
}
