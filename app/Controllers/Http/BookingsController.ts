import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import { Exception } from "@poppinss/utils";
import Booking from "App/Models/Booking";
import UserBooking from "App/Models/UserBooking";
import UpdateBookingValidator from "App/Validators/UpdateBookingValidator";

export default class BookingsController {
  /**
   * @swagger
   * /api/v1/bookings:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Menampilkan list booking semuanya
   *     parameters:
   *       - in: query
   *         name: fields_id
   *         description: id field dimana booking berada
   *         schema:
   *           type: integer
   *     responses:
   *       '200':
   *          description: Berhasil menampilkan list booking
   *       '5XX':
   *         description: Unexpected error.
   */
  public async index({ request, response }: HttpContextContract) {
    let whereClause = {};

    if (request.qs().fields_id) {
      whereClause["fields_id"] = request.qs().fields_id;
    }

    // *       - in: query
    // *         name: play_date
    // *         description: Tanggal main, format tanggal YYYY-MM-DD
    // *         schema:
    // *           type: string
    // *       - in: query
    // *         name: start
    // *         description: Jam main, format waktu hh:ii:ss
    // *         schema:
    // *           type: string

    // let play_date: string = "%%";
    // if (request.qs().play_date) {
    //   play_date = "%" + request.qs().play_date + "%";
    // }

    // let start: string = "%%";
    // if (request.qs().start) {
    //   start = "%" + request.qs().start + "%";
    // }

    try {
      const booking = await Booking.query()
        .where(whereClause)
        // .where("play_date_start", "LIKE", play_date)
        // .where("play_date_start", "LIKE", start)
        .preload("author")
        .preload("field", (query) => {
          query.select("id", "name", "type");
        })
        .preload("players", (query) => {
          query.select("id", "name", "email");
        })
        .withCount("players", (query) => {
          query.as("players_count");
        });

      return response.ok({
        status: "success",
        message: "Berhasil menampilkan list booking",
        data: booking,
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Gagal menampilkan list booking",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/bookings/{id}:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Menampilkan data booking by ID beserta pemainnya
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Berhasil get data booking by id
   *       '5XX':
   *         description: Unexpected error.
   */
  public async show({ params, response }: HttpContextContract) {
    try {
      const booking = await Booking.query()
        .where("id", params.id)
        .preload("author")
        .preload("field", (query) => {
          query.select("id", "name", "type");
        })
        .preload("players", (query) => {
          query.select("id", "name", "email");
        })
        .withCount("players", (query) => {
          query.as("players_count");
        })
        .firstOrFail();

      const {
        id,
        field_id,
        play_date_start,
        play_date_end,
        booking_user_id,
        author,
        players,
        field,
      } = booking.toJSON();
      const players_count = booking.$extras.players_count;

      return response.ok({
        status: "success",
        message: "Berhasil get data booking by id",
        data: {
          id,
          field_id,
          play_date_start,
          play_date_end,
          booking_user_id,
          players_count,
          players,
          author,
          field,
        },
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Gagal get data booking by id",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/bookings/{id}:
   *   put:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Mengubah data booking by ID
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     requestBody:
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             type: object
   *             properties:
   *               fields_id:
   *                 description: Field ID yang akan dibooking
   *                 type: integer
   *               play_date_start:
   *                 description: Tanggal dan Waktu mulai bermain, dalam format YYYY-mm-dd HH:MM:SS
   *                 type: datetime
   *               play_date_end:
   *                 description: Tanggal dan Waktu selesai bermain, dalam format YYYY-mm-dd HH:MM:SS
   *                 type: datetime
   *     responses:
   *       '200':
   *          description: Booking berhasil diperbarui
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '422':
   *          description: Booking milik orang lain
   *       '5XX':
   *         description: Unexpected error.
   */
  public async update({ auth, request, response }: HttpContextContract) {
    try {
      const { id } = request.params();
      const { fields_id, play_date_start, play_date_end } = request.body();

      await request.validate(UpdateBookingValidator);
      const booking = await Booking.findOrFail(id);

      const isSelfBooking = booking.users_id === auth.user!.id;

      if (!isSelfBooking) {
        throw new Exception(
          "Ini Booking milik orang lain, anda tidak memiliki hak!",
          422
        );
      }

      if (fields_id) {
        booking.fields_id = fields_id;
      }

      if (play_date_start) {
        booking.play_date_start = play_date_start;
      }

      if (play_date_end) {
        booking.play_date_end = play_date_end;
      }

      await booking.save();

      return response.ok({
        status: "success",
        message: "Booking berhasil diperbarui",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Booking gagal diperbarui",
          error: error.messages,
        });
      } else if (error.status == 422) {
        return response.unprocessableEntity({
          status: "failed",
          message: error.message,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Booking gagal diperbarui",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/bookings/{id}:
   *   delete:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Menghapus data booking by ID
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Berhasil get data booking by id
   *       '422':
   *          description: Booking milik orang lain
   *       '5XX':
   *         description: Unexpected error.
   */
  public async destroy({ auth, request, response }: HttpContextContract) {
    try {
      const { id } = request.params();
      const booking = await Booking.findOrFail(id);

      const isSelfBooking = booking.users_id === auth.user!.id;

      if (!isSelfBooking) {
        throw new Exception(
          "Ini Booking milik orang lain, anda tidak memiliki hak!",
          422
        );
      }
      await booking.delete();

      return response.ok({
        status: "success",
        message: "Booking berhasil dihapus",
      });
    } catch (error) {
      if (error.status == 422) {
        return response.unprocessableEntity({
          status: "failed",
          message: error.message,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Booking gagal dihapus",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/bookings/{id}/join:
   *   put:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Melakukan join booking
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Berhasil join booking
   *       '422':
   *          description: Anda sudah join booking ini
   *       '5XX':
   *         description: Unexpected error.
   */
  public async join({ auth, params, response }: HttpContextContract) {
    try {
      const { id } = params;
      const booking = await Booking.findOrFail(id);
      const user = auth.user!;

      const isAlreadyJoin = await UserBooking.query()
        .where({
          bookings_id: id,
          users_id: user.id,
        })
        .first();

      if (isAlreadyJoin) {
        throw new Exception("Anda sudah join booking ini", 422);
      }

      await booking.related("players").attach([user.id]);

      return response.ok({
        status: "success",
        message: "berhasil join booking",
      });
    } catch (error) {
      if (error.status == 422) {
        return response.unprocessableEntity({
          status: "failed",
          message: error.message,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Gagal join booking",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/bookings/{id}/unjoin:
   *   put:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Mambatalkan join booking
   *     parameters:
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Berhasil membatalkan join booking
   *       '422':
   *          description: Anda belum join booking ini
   *       '5XX':
   *         description: Unexpected error.
   */
  public async unjoin({ auth, params, response }: HttpContextContract) {
    try {
      const { id } = params;
      const booking = await Booking.findOrFail(id);
      const user = auth.user!;

      const isNotAlreadyJoin = await UserBooking.query()
        .where({
          bookings_id: id,
          users_id: user.id,
        })
        .first();

      if (!isNotAlreadyJoin) {
        throw new Exception("Anda belum join booking ini", 422);
      }

      await booking.related("players").detach([user.id]);

      return response.ok({
        status: "success",
        message: "berhasil membatalkan join booking",
      });
    } catch (error) {
      if (error.status == 422) {
        return response.unprocessableEntity({
          status: "failed",
          message: error.message,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Gagal membatalkan join booking",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/schedules:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Booking
   *     summary: Menampilkan list booking yang diikuti oleh user yang sedang melakukan login
   *     responses:
   *       '200':
   *          description: Berhasil menampilkan list booking
   *       '5XX':
   *         description: Unexpected error.
   */
  public async schedules({ auth, response }: HttpContextContract) {
    try {
      const { id } = auth.user!;
      const booking = await Booking.query()
        .where("users_id", id)
        .preload("field", (query) => {
          query.select("id", "name", "type");
        })
        .preload("players", (query) => {
          query.select("id", "name", "email");
        })
        .withCount("players", (query) => {
          query.as("players_count");
        });

      return response.ok({
        status: "success",
        message: "Berhasil menampilkan list booking",
        data: booking,
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Gagal menampilkan list booking",
        error: error.message,
      });
    }
  }
}
