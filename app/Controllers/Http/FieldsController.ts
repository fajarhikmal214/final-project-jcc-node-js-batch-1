import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Field from "App/Models/Field";
import Venue from "App/Models/Venue";
import CreateFieldValidator from "App/Validators/CreateFieldValidator";
import UpdateFieldValidator from "App/Validators/UpdateFieldValidator";

export default class FieldsController {
  /**
   * @swagger
   * /api/v1/venues/{venue_id}/fields:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Field
   *     summary: Menampilkan semua data fields berdasarkan venue_id
   *     parameters:
   *       - in: path
   *         name: venue_id
   *         schema:
   *           type: integer
   *         required: true
   *       - in: query
   *         name: name
   *         schema:
   *           type: string
   *       - in: query
   *         name: type
   *         description: Tipe atau Kategori Lapangan yang ingin di filter
   *         schema:
   *           type: string
   *           description: "Allowed types: soccer, minisoccer, futsal, basketball, volleyball"
   *           enum: [soccer, minisoccer, futsal, basketball, volleyball]
   *     responses:
   *       '200':
   *          description: Fields berhasil dimuat
   *       '5XX':
   *         description: Unexpected error.
   */
  public async index({ params, request, response }: HttpContextContract) {
    try {
      const { venue_id } = params;

      let whereType = {};
      if (request.qs().type) {
        whereType = {
          type: request.qs().type,
        };
      }

      if (request.qs().name) {
        const fields = await Field.query()
          .where("name", "like", `%${request.qs().name}%`)
          .where("venues_id", venue_id)
          .where(whereType)
          .preload("bookings", (query) => {
            query.withCount("players", (query) => {
              query.as("players_count");
            });
          });

        return response.ok({
          status: "success",
          message: "Fields berhasil dimuat",
          data: fields,
        });
      } else {
        const fields = await Field.query()
          .where("venues_id", venue_id)
          .where(whereType)
          .preload("bookings", (query) => {
            query.withCount("players", (query) => {
              query.as("players_count");
            });
          });

        return response.ok({
          status: "Fields",
          message: "Fields berhasil dimuat",
          data: fields,
        });
      }
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Fields gagal dimuat",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{venue_id}/fields:
   *   post:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Field
   *     summary: Membuat data field baru
   *     parameters:
   *       - in: path
   *         name: venue_id
   *         schema:
   *           type: integer
   *         required: true
   *     requestBody:
   *       required: true
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             $ref: '#definitions/Field'
   *     responses:
   *       '201':
   *          description: Field berhasil ditambahkan
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async store({ params, request, response }: HttpContextContract) {
    try {
      const { venue_id } = params;
      const { name, type } = request.body();

      const venue = await Venue.find(venue_id);

      if (!venue) {
        return response.notFound({
          message: "venue_id tidak tersedia",
        });
      }

      await request.validate(CreateFieldValidator);

      const field = new Field();
      field.name = name;
      field.type = type;
      await field.related("venue").associate(venue);

      return response.created({
        status: "success",
        message: "Berhasil menambahkan data field baru",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          messages: "Field gagal ditambahkan",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          messages: "Field gagal ditambahkan",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{venue_id}/fields/{id}:
   *   get:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Field
   *     summary: Menampilkan data field by id
   *     parameters:
   *       - in: path
   *         name: venue_id
   *         schema:
   *           type: number
   *         required: true
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Field berhasil dimuat
   *       '5XX':
   *         description: Unexpected error.
   */
  public async show({ params, response }: HttpContextContract) {
    try {
      const { venue_id, id } = params;
      const field = await Field.query()
        .where("id", id)
        .where("venues_id", venue_id)
        .preload("bookings", (query) => {
          query.withCount("players", (query) => {
            query.as("players_count");
          });
        })
        .firstOrFail();

      return response.ok({
        status: "success",
        message: "Field berhasil dimuat",
        data: field,
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        messages: "Fields gagal dimuat",
        error: error.message,
      });
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{venue_id}/fields/{id}:
   *   put:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Field
   *     summary: Mengubah data field by ID
   *     parameters:
   *       - in: path
   *         name: venue_id
   *         schema:
   *           type: number
   *         required: true
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     requestBody:
   *       required: true
   *       content:
   *         application/x-www-form-urlencoded:
   *           schema:
   *             $ref: '#definitions/Field'
   *     responses:
   *       '201':
   *          description: Fields berhasil diperbarui
   *       '400':
   *         description: Bad request. Request tidak sesuai dengan validasi.
   *       '5XX':
   *         description: Unexpected error.
   */
  public async update({ params, request, response }: HttpContextContract) {
    try {
      const { venue_id, id } = params;
      const { name, type } = request.body();

      const venue = await Venue.find(venue_id);

      if (!venue) {
        return response.notFound({
          message: "venue_id tidak tersedia",
        });
      }

      await request.validate(UpdateFieldValidator);

      const field = await Field.findOrFail(id);
      field.name = name;
      field.type = type;
      await field.related("venue").associate(venue);

      return response.ok({
        status: "success",
        message: "Field berhasil diperbarui",
      });
    } catch (error) {
      if (error.code === "E_VALIDATION_FAILURE") {
        return response.badRequest({
          status: "failed",
          message: "Field gagal diperbarui",
          error: error.messages,
        });
      } else {
        return response.badGateway({
          status: "failed",
          message: "Field gagal diperbarui",
          error: error.message,
        });
      }
    }
  }

  /**
   * @swagger
   * /api/v1/venues/{venue_id}/fields/{id}:
   *   delete:
   *     security:
   *       - bearerAuth: []
   *     tags:
   *       - Field
   *     summary: Menghapus data field by id
   *     parameters:
   *       - in: path
   *         name: venue_id
   *         schema:
   *           type: number
   *         required: true
   *       - in: path
   *         name: id
   *         schema:
   *           type: integer
   *         required: true
   *     responses:
   *       '200':
   *          description: Field berhasil dihapus
   *       '5XX':
   *         description: Unexpected error.
   */
  public async destroy({ params, response }: HttpContextContract) {
    try {
      const { venue_id, id } = params;

      const venue = await Venue.find(venue_id);

      if (!venue) {
        return response.notFound({
          message: "venue_id tidak tersedia",
        });
      }

      let field = await Field.findOrFail(id);
      await field.delete();

      return response.ok({
        status: "success",
        message: "Field berhasil dihapus",
      });
    } catch (error) {
      return response.badGateway({
        status: "failed",
        message: "Field gagal diperbarui",
        error: error.message,
      });
    }
  }
}
