import { DateTime } from "luxon";
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  hasMany,
  HasMany,
} from "@ioc:Adonis/Lucid/Orm";
import Field from "App/Models/Field";
import User from "./User";

/**
 *  @swagger
 *  definitions:
 *    Venue:
 *      type: object
 *      properties:
 *        id:
 *          type: uint
 *        name:
 *          type: string
 *        address:
 *          type: string
 *        phone:
 *          type: string
 *      required:
 *        - name
 */

export default class Venue extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public name: string;

  @column()
  public address: string;

  @column()
  public phone: string;

  @column()
  public users_id: number;

  @column.dateTime({
    autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public updatedAt: DateTime;

  @belongsTo(() => User, {
    foreignKey: "users_id",
    onQuery(query) {
      query.select("id", "name", "email");
    },
  })
  public author: BelongsTo<typeof User>;

  @hasMany(() => Field, {
    foreignKey: "venues_id",
    onQuery(query) {
      query.select("id", "name", "type");
    },
  })
  public fields: HasMany<typeof Field>;
}
