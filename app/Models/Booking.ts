import { DateTime } from "luxon";
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  ManyToMany,
  manyToMany,
} from "@ioc:Adonis/Lucid/Orm";
import User from "./User";
import Field from "./Field";

/**
 *  @swagger
 *  definitions:
 *    Booking:
 *      type: object
 *      properties:
 *        id:
 *          type: uint
 *        fields_id:
 *          type: int
 *        play_date_start:
 *          type: datetime
 *        play_date_end:
 *          type: datetime
 *      required:
 *        - fields_id
 *        - play_date_start
 *        - play_date_end
 */

export default class Booking extends BaseModel {
  public serializeExtras = true;

  @column({ isPrimary: true })
  public id: number;

  @column.dateTime({
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public play_date_start: DateTime;

  @column.dateTime({
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public play_date_end: DateTime;

  @column()
  public users_id: number;

  @column()
  public fields_id: number;

  @column.dateTime({
    autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public updatedAt: DateTime;

  @belongsTo(() => User, {
    foreignKey: "users_id",
    onQuery(query) {
      query.select("id", "name", "email");
    },
  })
  public author: BelongsTo<typeof User>;

  @belongsTo(() => Field, {
    foreignKey: "fields_id",
  })
  public field: BelongsTo<typeof Field>;

  @manyToMany(() => User, {
    pivotTable: "users_has_bookings",
    pivotForeignKey: "bookings_id",
    pivotRelatedForeignKey: "users_id",
  })
  public players: ManyToMany<typeof User>;
}
