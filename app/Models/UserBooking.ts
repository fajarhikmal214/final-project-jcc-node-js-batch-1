import { BaseModel, column } from "@ioc:Adonis/Lucid/Orm";

export default class UserBooking extends BaseModel {
  public static table = "users_has_bookings";

  @column({ isPrimary: true })
  public id: number;

  @column()
  public users_id: number;

  @column()
  public bookings_id: number;
}
