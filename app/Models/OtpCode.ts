import { DateTime } from "luxon";
import { BaseModel, BelongsTo, belongsTo, column } from "@ioc:Adonis/Lucid/Orm";
import User from "./User";

export default class OtpCode extends BaseModel {
  public static table = "otp_codes";

  @column({ isPrimary: true })
  public id: number;

  @column()
  public otp_code: number;

  @column()
  public users_id: number;

  @column.dateTime({
    autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public updatedAt: DateTime;

  @belongsTo(() => User, {
    foreignKey: "users_id",
  })
  public owner: BelongsTo<typeof User>;
}
