import { DateTime } from "luxon";
import {
  BaseModel,
  BelongsTo,
  belongsTo,
  column,
  HasMany,
  hasMany,
} from "@ioc:Adonis/Lucid/Orm";
import Booking from "./Booking";
import Venue from "./Venue";

/**
 *  @swagger
 *  definitions:
 *    Field:
 *      type: object
 *      properties:
 *        id:
 *          type: uint
 *        name:
 *          type: string
 *        type:
 *          type: string
 *          description: "Allowed types: soccer, minisoccer, futsal, basketball, volleyball"
 *          enum: [soccer, minisoccer, futsal, basketball, volleyball]
 *      required:
 *        - name
 *        - type
 */
export default class Field extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column()
  public name: string;

  @column()
  public type: Array<string> = [
    "soccer",
    "minisoccer",
    "futsal",
    "basketball",
    "volleyball",
  ];

  @column()
  public venues_id: number;

  @column.dateTime({
    autoCreate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public createdAt: DateTime;

  @column.dateTime({
    autoCreate: true,
    autoUpdate: true,
    serialize: (value: DateTime | null) => {
      return value ? value.toFormat("dd-MM-yyyy HH:mm:ss") : value;
    },
  })
  public updatedAt: DateTime;

  @belongsTo(() => Venue, {
    foreignKey: "venues_id",
  })
  public venue: BelongsTo<typeof Venue>;

  @hasMany(() => Booking, {
    foreignKey: "fields_id",
    onQuery(query) {
      query.select(
        "id",
        "fields_id",
        "play_date_start",
        "play_date_end",
        "users_id"
      );
    },
  })
  public bookings: HasMany<typeof Booking>;
}
