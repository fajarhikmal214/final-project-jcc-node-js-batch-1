import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class FieldBookingValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    fields_id: schema.number([rules.exists({ table: "fields", column: "id" })]),
    play_date_start: schema.date({
      format: "yyyy-MM-dd HH:mm:ss",
    }),
    play_date_end: schema.date(
      {
        format: "yyyy-MM-dd HH:mm:ss",
      },
      [rules.afterField("play_date_start")]
    ),
  });

  public messages = {
    "fields_id.required": "Kolom {{ field }} harus diisi",
    "fields_id.number": "Kolom {{ field }} harus berupa number",
    "fields_id.exists": "Kolom {{ field }} tidak tersedia",
    "play_date_start.required": "Kolom {{ field }} harus diisi",
    "play_date_start.date": "Kolom {{ field }} harus berupa tanggal",
    "play_date_start.date.format":
      "Kolom {{ field }} harus memiliki format yyyy-MM-dd HH:mm:ss",
    "play_date_end.required": "Kolom {{ field }} harus diisi",
    "play_date_end.date": "Kolom {{ field }} harus berupa tanggal",
    "play_date_end.date.format":
      "Kolom {{ field }} harus memiliki format yyyy-MM-dd HH:mm:ss",
    "play_date_end.afterField":
      "Kolom {{ field }} harus lebih beberapa saat dari play_date_start",
  };
}
