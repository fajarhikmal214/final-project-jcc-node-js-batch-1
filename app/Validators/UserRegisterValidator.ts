import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class UserRegisterValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    name: schema.string({}, []),
    email: schema.string({}, [
      rules.email(),
      rules.unique({
        table: "users",
        column: "email",
      }),
    ]),
    password: schema.string({}, [rules.minLength(8)]),
    role: schema.enum(["user", "owner"]),
  });

  public messages = {
    "name.required": "Kolom name harus diisi",
    "name.string": "Kolom name harus berupa string",
    "email.required": "Kolom email harus diisi",
    "email.string": "Kolom email harus berupa string",
    "email.email": "Format email harus valid",
    "email.unique": "Email ini sudah terdaftar",
    "password.required": "Kolom password harus diisi",
    "password.string": "Kolom password harus berupa string",
    "password.minLength": "Kolom password minimal 8 karakter",
    "role.required": "Kolom role harus diisi",
    "role.enum": "Kolom role hanya dapat diisi dengan pilihan yang tersedia",
  };
}
