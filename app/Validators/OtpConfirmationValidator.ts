import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class OtpConfirmationValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    otp_code: schema.number(),
    email: schema.string({}, [
      rules.email(),
      rules.exists({ table: "users", column: "email" }),
    ]),
  });

  public messages = {
    "otp_code.required": "Kolom {{ field }} harus diisi",
    "otp_code.number": "Kolom {{ field }} harus berupa number",
    "email.required": "Kolom email harus diisi",
    "email.string": "Kolom email harus berupa string",
    "email.email": "Format email harus valid",
    "email.exists": "Email ini belum terdaftar",
  };
}
