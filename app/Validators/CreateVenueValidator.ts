import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class CreateVenueValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    name: schema.string({}, []),
    address: schema.string.optional({}, []),
    phone: schema.string.optional({}, [
      rules.mobile({
        locales: ["id-ID"],
      }),
    ]),
  });

  public messages = {
    "name.required": "Kolom name harus diisi",
    "name.string": "Kolom name harus berupa string",
    "address.string": "Kolom address harus berupa string",
    "phone.string": "Kolom phone harus berupa string",
    "phone.mobile": "Kolom phone harus berupa nomor telepon",
  };
}
