import { schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class UpdateFieldValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    name: schema.string.optional({}, []),
    type: schema.enum.optional([
      "soccer",
      "minisoccer",
      "futsal",
      "basketball",
      "volleyball",
    ]),
  });

  public messages = {
    "name.string": "Kolom name harus berupa string",
    "type.enum": "Kolom type hanya dapat diisi dengan pilihan yang tersedia",
  };
}
