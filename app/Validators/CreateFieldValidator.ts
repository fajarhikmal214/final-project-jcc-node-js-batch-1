import { schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class CreateFieldValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    name: schema.string({}, []),
    type: schema.enum([
      "soccer",
      "minisoccer",
      "futsal",
      "basketball",
      "volleyball",
    ]),
  });

  public messages = {
    "name.required": "Kolom name harus diisi",
    "name.string": "Kolom name harus berupa string",
    "type.required": "Kolom type harus diisi",
    "type.enum": "Kolom type hanya dapat diisi dengan pilihan yang tersedia",
  };
}
