import { schema, rules } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class CreateBookingValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    nama_pembooking: schema.string({}, []),
    nama_venue: schema.string({}, []),
    tanggal: schema.date({}, [rules.after("today")]),
  });

  public messages = {
    "nama_pembooking.required": "Kolom Nama Pem-booking harus diisi",
    "nama_pembooking.string": "Kolom Nama Pem-booking harus berupa string",
    "nama_venue.required": "Kolom Nama Venue harus diisi",
    "nama_venue.string": "Kolom Nama Venue harus berupa string",
    "tanggal.required": "Kolom Tanggal harus diisi",
    "tanggal.date.format": "Kolom Tanggal harus berupa tanggal",
    "tanggal.after": "Kolom Tanggal harus diisi minimal 1 hari sebelum",
  };
}
