import { schema } from "@ioc:Adonis/Core/Validator";
import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class UserLoginValidator {
  constructor(protected ctx: HttpContextContract) {}

  public schema = schema.create({
    email: schema.string({}, []),
    password: schema.string({}, []),
  });

  public messages = {
    "email.required": "Kolom email harus diisi",
    "email.string": "Kolom email harus berupa string",
    "password.required": "Kolom password harus diisi",
    "password.string": "Kolom password harus berupa string",
  };
}
