import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class Verify {
  public async handle(
    { auth, response }: HttpContextContract,
    next: () => Promise<void>
  ) {
    const isVerify = auth.user!.is_verified;
    if (!isVerify) {
      response.forbidden({
        status: "failed",
        message:
          "Anda tidak dapat mengakses endpoint ini, silakan lakukan verifikasi akun terlebih dahulu",
      });
      return;
    }

    await next();
  }
}
