import { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

export default class Acl {
  public async handle(
    { auth, response }: HttpContextContract,
    next: () => Promise<void>,
    allowedRoles
  ) {
    const userRole = auth.user?.role;
    if (!allowedRoles.includes(userRole)) {
      response.forbidden({
        status: "failed",
        message: "Anda tidak memiliki hak untuk mengakses endpoint ini!",
      });
      return;
    }
    await next();
  }
}
