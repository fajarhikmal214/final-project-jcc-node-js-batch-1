import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Venues extends BaseSchema {
  protected tableName = "venues";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("name", 255).notNullable();
      table.text("address").nullable();
      table.string("phone", 45).nullable();
      table
        .integer("users_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("users")
        .onDelete("CASCADE");
      table.timestamp("created_at", { useTz: true });
      table.timestamp("updated_at", { useTz: true });
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(["users_id"]);
    });
    this.schema.dropTable(this.tableName);
  }
}
