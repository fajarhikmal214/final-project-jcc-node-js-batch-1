import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Bookings extends BaseSchema {
  protected tableName = "bookings";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.dateTime("play_date_start").notNullable();
      table.dateTime("play_date_end").notNullable();
      table
        .integer("users_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("users")
        .onDelete("CASCADE");
      table
        .integer("fields_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("fields")
        .onDelete("CASCADE");
      table.timestamp("created_at", { useTz: true });
      table.timestamp("updated_at", { useTz: true });
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(["users_id"]);
      table.dropForeign(["fields_id"]);
    });
    this.schema.dropTable(this.tableName);
  }
}
