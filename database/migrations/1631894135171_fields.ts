import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class Fields extends BaseSchema {
  protected tableName = "fields";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table.string("name", 255).notNullable();
      table
        .enum("type", [
          "soccer",
          "minisoccer",
          "futsal",
          "basketball",
          "volleyball",
        ])
        .notNullable();
      table
        .integer("venues_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("venues")
        .onDelete("CASCADE");
      table.timestamp("created_at", { useTz: true });
      table.timestamp("updated_at", { useTz: true });
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(["venues_id"]);
    });
    this.schema.dropTable(this.tableName);
  }
}
