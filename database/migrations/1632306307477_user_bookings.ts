import BaseSchema from "@ioc:Adonis/Lucid/Schema";

export default class UserBookings extends BaseSchema {
  protected tableName = "users_has_bookings";

  public async up() {
    this.schema.createTable(this.tableName, (table) => {
      table.increments("id");
      table
        .integer("users_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("users")
        .onDelete("CASCADE");
      table
        .integer("bookings_id")
        .notNullable()
        .unsigned()
        .references("id")
        .inTable("bookings")
        .onDelete("CASCADE");
    });
  }

  public async down() {
    this.schema.table(this.tableName, (table) => {
      table.dropForeign(["users_id"]);
      table.dropForeign(["bookings_id"]);
    });
    this.schema.dropTable(this.tableName);
  }
}
